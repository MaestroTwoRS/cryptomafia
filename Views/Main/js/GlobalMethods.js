﻿getCookie=function (name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

send_form_register = function () { 
    var msg = $('#form_register').serialize();
    $.ajax({
        type: 'POST',
        url: 'api/register',
        data: msg,
        success: function (data) {
            $('#results').html(data);
        },
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
};  


var GlobalMethods = {

    Register: function (mail, password, name, captcha) {  
        $.post("api/register", { Mail: mail, Password: password, Name:name, Captcha: captcha }, function (data) {
            if (data === "1") {
                GD.Register.RegisterSuccess=true; 
            } else {
                if (data === "2"){ 
                    GD.Register.ErrorMessage = "Эта почта уже занята";
                } else { 
                    GD.Register.ErrorMessage = data;
                }
                 
            }
        });
    },
    Login: function (login, password) { 
        $.post("api/auth", { Email: login.toLowerCase(), Password: password}, function (data) {
            if (data === "1") {
                GD.Auth.IsLoginFalse = 0;
                window.location = "/";
            } else { 
                GD.Auth.IsLoginFalse = 1; 
            }
        });
    },

    send_new_pay: function () {
        var postData = $("#newPay_form").serializeArray();
        $.ajax(
            {
                url: "/api_user/sendpayinfo",
                type: "POST",
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    if (data == 1) {
                        GD.New_Pay.IsSuccess = true; 
                    };
                    if (data == 0) {
                        GD.New_Pay.IsSuccess = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    GD.New_Pay.IsSuccess = false; 
                }
            }); 
    },
};


