﻿function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

function formatDate(d) {
    var date = new Date(d);
    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    return dd + '.' + mm + '.' + yy;
}; 

var GD = {
    UsersTable: [],
    User: {
        SelectedUserId: 0,
        PriceToVIPadd: 10,
        TimeToVIPadd: { value: 1 },
        DateBeginVIP: null,
        DateEndVIP: null,
        DaysForVIP: null,
        DateEndVIPtext: null,
        PriceDay: null,
        UserInfoTab: 1
    },
    New_Pay: {
        MyPurse: "1111111",
        Money: "",
        Paket: "",
        myPurseType: "BTC",
        myPurseType_options: [
            { text: 'BTC', value: 'BTC' },
            { text: 'ETH', value: 'ETH' }
        ],
        IsSuccess: null
    },
    Transaktions: {
        SelectedTransaktion: '',
        UserName: "Name",
        UserMoney: 11111111,
        UserPaket: 1,
        UserDeskription: "text",
        VipTime:"time- time"
    },
    PaysTable: {},
    PaysTableInfo: {},
    WS: {
        StatusWord: "Подключение.....",
        Status: false
    }
};
 

 
