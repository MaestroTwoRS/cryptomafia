﻿var GlobalMethods = {
    GetUsersList: function () {
        $.ajax({
            url: '/api_admin/GetUserForAdmin',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            type: 'GET', data: {},
            success: function (data) {
                GD.UsersTable = JSON.parse(data);
            },
            error: function (xhr, status, errorThrown) {
            }
        }); 
    },
    GetNullPaysCount: function () {
        x = 0;
        _.each(GD.PaysTable, function (elem) {
            if (elem.admin_answer=="") { x++ }
        });
        if (x == 0) { return ""; }
        return x;
    },
    GetPaysList: function () {
        $.ajax({
            url: '/api_admin/GetPaysForAdmin',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            type: 'GET', data: {},
            success: function (data) {
                GD.PaysTable = JSON.parse(data);
                _.each(GD.PaysTable, function (elem) {
                    elem.date_begin = elem.date_begin.substr(0, elem.date_begin.length - 8);
                    elem.date_end = elem.date_end.substr(0, elem.date_end.length - 8);
                });
            },
            error: function (xhr, status, errorThrown) {
            }
        });
    }, 
    TransaktionChange: function (transaktion_table, answer) { 
        if (transaktion_table.admin_answer==1){ alert("Нельзя изменить значение, если Вы уже приняли положительное решение!");return}
        var transaktion_id = transaktion_table.id;
        $.ajax({
            url: '/api_admin/TransaktionChange',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }, data: { "transaktion_id": transaktion_id, "answer": answer },
            type: 'POST',  
            success: function (data) {
                if (data == "0") { alert("Вы приняли уже положительное решение. Его нельзя отменить"); return }
                $.ajax({
                    url: '/api_admin/GetPaysForAdmin',
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    type: 'GET', data: {},
                    success: function (data) {
                        GD.PaysTable = JSON.parse(data);
                        _.each(GD.PaysTable, function (elem) {
                            elem.date_begin = elem.date_begin.substr(0, elem.date_begin.length - 8);
                            elem.date_end = elem.date_end.substr(0, elem.date_end.length - 8);
                        });
                    },
                    error: function (xhr, status, errorThrown) {
                    }
                });
            },
            error: function (xhr, status, errorThrown) {
            }
        });
    }, 
    SelectUser: function (id) {
        router.push({ query: { user_tab: 1,id:id } });
        GD.User.SelectedUserId = id;
    }, 
    GetUndefinedBool: function (rout, query_param, param2) {
        try { if (rout.query[query_param] === param2) { return true; } } catch (e) { return false; }
    },
    UserSelectionChanged: function (rout) { 
        if (rout.query['id'] === undefined) { return null; }
        GD.User.SelectedUserId = rout.query['id'];
    },
    TransaktionSelectionChanged: function (trans_id) { 
        GD.PaysTableInfo = {},
        GD.New_Pay.IsSuccess = null;   
        if (trans_id === undefined) { }
        else { 
            GD.User.SelectedUserId = trans_id;
            router.push({ query: { transaktion: trans_id} });
            GD.Transaktions.SelectedTransaktion = trans_id; 
            $.ajax(
                {
                    url: "/api_admin/gettransaktion",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    type: "POST", data: { "transaktion_id": trans_id},
                    success: function (data, textStatus, jqXHR) {
                        var x = JSON.parse(data)
                        GD.PaysTableInfo = x[0]; 
                        GD.PaysTableInfo.date_begin = GD.PaysTableInfo.date_begin.substr(0, GD.PaysTableInfo.date_begin.length - 8); 
                        GD.PaysTableInfo.date_end = GD.PaysTableInfo.date_end.substr(0, GD.PaysTableInfo.date_end.length - 8);  
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Произошла какая-то ошибка!");
                    }
                }); 
        }
        
    },
    /*SetVIP: function () {
        $.post("/api_admin/new_vip", { UserId: GD.User.SelectedUserId, BeginTime: GD.User.DateBeginVIP, Money: GD.User.PriceToVIPadd,Paket: GD.User.TimeToVIPadd.value }, function (data) {
            alert(data);
        }); 
    },*/ 
    Users_User_Tab_change: function (index) {
        router.push({ query: { user_tab: index } });
    }, 

    send_new_vip: function (index) {
        var postData = $("#New_Pay_form").serializeArray();
        $.ajax(
            {
                url: "/api_admin/sendpay",
                type: "POST",
                data: postData,
                success: function (data, textStatus, jqXHR) {
                    if (data == 1) {
                        GD.New_Pay.IsSuccess = true;
                        $.ajax({
                            url: '/api_admin/GetPaysForAdmin',
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            type: 'GET', data: {},
                            success: function (data) {
                                GD.PaysTable = JSON.parse(data); 
                            },
                            error: function (xhr, status, errorThrown) {
                            }
                        });

                    } else { GD.New_Pay.IsSuccess = false; }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    GD.New_Pay.IsSuccess = false;
                }
            }); 
    }, 
    ExecuteTimeMoneyForVIP: function (dat, viptime, money) { 
        if (dat === undefined || dat === null || viptime === null) { return; } 
        var date = new Date(dat); 
        var enddate = new Date(dat); 
        var days = 0;
        var d = new Date();
        var begin_date = date.getDate();
        var begin_month = date.getMonth() + 1;
        var begin_year = date.getFullYear();

        GD.User.DateBeginVIP =begin_year + "-" + begin_month + "-" + begin_date;
        
        var textDate; 
        switch (viptime) {
            case 1: 
                enddate.setDate(enddate.getDate() + 30); 
                GD.User.DateEndVIP = (enddate.getDate() < 10 ? '0' + enddate.getDate() : enddate.getDate()) + '.' + (enddate.getMonth() < 9 ? '0' + (enddate.getMonth() + 1) : enddate.getMonth() + 1) + '.' + enddate.getFullYear();
                days = 30;
                break;
            case 2: 
                enddate.setDate(enddate.getDate() + 65); 
                GD.User.DateEndVIP = (enddate.getDate() < 10 ? '0' + enddate.getDate() : enddate.getDate()) + '.' + (enddate.getMonth() < 9 ? '0' + (enddate.getMonth() + 1) : enddate.getMonth() + 1) + '.' + enddate.getFullYear();
                days = 65;
                break;
            case 3: 
                enddate.setDate(enddate.getDate() + 100); 
                GD.User.DateEndVIP = (enddate.getDate() < 10 ? '0' + enddate.getDate() : enddate.getDate()) + '.' + (enddate.getMonth() < 9 ? '0' + (enddate.getMonth() + 1) : enddate.getMonth() + 1) + '.' + enddate.getFullYear();
                days = 100;
                break; 
        }
        GD.User.DaysForVIP = days;
        GD.User.PriceDay = (money / days);

        



        //var N = 1;
        //date_.setMonth(date_.getMonth() + N);
        //alert(date_);
        
    }

    
};  
        

